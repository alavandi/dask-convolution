**This file is here to document the state of the project and the future changes and additions that could happen in the near future**. 


##### Ideas list :
* Adding direct convolution as an option : **Not started**.
* Adding auto as the default method using choose_cv_method from scipy : **Not started**.
* Working out the case where both inputs are Dask arrays and using parrellism in that case : **Not started**. 
* Giving the ability to have the mode depends on the axis with a tuple or a dict : **Not started**.
